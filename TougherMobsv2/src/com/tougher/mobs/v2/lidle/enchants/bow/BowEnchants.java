package com.tougher.mobs.v2.lidle.enchants.bow;

import org.bukkit.enchantments.Enchantment;

public enum BowEnchants {

	ARROW_DAMAGE(Enchantment.ARROW_DAMAGE, Enchantment.ARROW_DAMAGE.getMaxLevel()),
	ARROW_FIRE(Enchantment.ARROW_FIRE, Enchantment.ARROW_FIRE.getMaxLevel()),
	ARROW_INFINITE(Enchantment.ARROW_INFINITE, Enchantment.ARROW_INFINITE.getMaxLevel()),
	ARROW_KNOCKBACK(Enchantment.ARROW_KNOCKBACK, Enchantment.ARROW_KNOCKBACK.getMaxLevel()),
	DURABILITY(Enchantment.DURABILITY, Enchantment.DURABILITY.getMaxLevel());
	
	private Enchantment enchantment;
	private int maxLevel;
	
	public static final int SIZE = 5;
	
	BowEnchants(Enchantment enchantment, int maxLevel){
		this.enchantment = enchantment;
		this.maxLevel = maxLevel;
	}

	public Enchantment getEnchantment() {
		return enchantment;
	}

	public int getMaxLevel() {
		return maxLevel;
	}
	
}
