package com.tougher.mobs.v2.lidle.enchants.weapons;

import org.bukkit.enchantments.Enchantment;

public enum WeaponEnchants {

	DAMAGE_ALL(Enchantment.DAMAGE_ALL, Enchantment.DAMAGE_ALL.getMaxLevel()),
	DAMAGE_ARTHROPODS(Enchantment.DAMAGE_ARTHROPODS, Enchantment.DAMAGE_ARTHROPODS.getMaxLevel()),
	DAMAGE_UNDEAD(Enchantment.DAMAGE_UNDEAD, Enchantment.DAMAGE_UNDEAD.getMaxLevel()),
	DURABILITY(Enchantment.DURABILITY, Enchantment.DURABILITY.getMaxLevel()),
	FIRE_ASPECT(Enchantment.FIRE_ASPECT, Enchantment.FIRE_ASPECT.getMaxLevel()),
	KNOCKBACK(Enchantment.KNOCKBACK, Enchantment.KNOCKBACK.getMaxLevel()),
	LOOT_BONUS_MOBS(Enchantment.LOOT_BONUS_MOBS, Enchantment.LOOT_BONUS_MOBS.getMaxLevel()),
	SILK_TOUCH(Enchantment.SILK_TOUCH, Enchantment.SILK_TOUCH.getMaxLevel());
	
	private Enchantment enchantment;
	private int maxLevel;
	
	public static final int SIZE = 8;
	
	WeaponEnchants(Enchantment enchantment, int maxLevel){
		this.enchantment = enchantment;
		this.maxLevel = maxLevel;
	}

	public Enchantment getEnchantment() {
		return enchantment;
	}

	public int getMaxLevel() {
		return maxLevel;
	}
		
}
