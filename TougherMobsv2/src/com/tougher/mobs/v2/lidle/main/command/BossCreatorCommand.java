package com.tougher.mobs.v2.lidle.main.command;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import com.tougher.mobs.v2.lidle.data.MobValues;
import com.tougher.mobs.v2.lidle.main.TougherMobs;

public class BossCreatorCommand implements CommandExecutor {

	public static final String PATH_NAME = "plugins//TougherMobs//Bosses//";
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			sender.sendMessage("Only players can use this command!");
			return false;
		}

		//Player does not have permission to use the command
		if(!(sender.hasPermission(TougherMobs.bossCreator))){
			sender.sendMessage(ChatColor.RED + "You do not have permission to use that command!");
			return false;
		}
		
		//Everything below here corresponds to command cases
		
		else if(cmd.getName().equalsIgnoreCase("createboss")){
			Player p = (Player) sender;
			if(args.length >= 1){
				
				//set name
				if(args[0].equalsIgnoreCase("setname")){
					// /createboss setname <boss name>
					if(args.length == 2){
						String name = args[1];
						setBossName(name);
						sender.sendMessage(ChatColor.GREEN + name + " created!");
						return true;
					}
					
					else{
						invalidParameters(p);
						return false;
					}
					
				}
				
				//set type
				else if (args[0].equalsIgnoreCase("settype")){
					// /createboss settype <boss name> <boss type>
					if(args.length == 3){
						String bossName = args[1];
						EntityType type = getEntityTypeFromString(args[2]);
						
						if(type == null){
							sender.sendMessage(ChatColor.RED + "Invalid type! Using Zombie as default!");
							type = EntityType.ZOMBIE;
						}
						
						setBossType(bossName, type);
						p.sendMessage(ChatColor.GREEN + "Set " + bossName + " to entity type: " + type.toString());
						return true;
					}
					
					else{
						invalidParameters(p);
						return false;
					}
				}
				
				//set drops
				else if (args[0].equalsIgnoreCase("setdrop")){
					// /createboss setdrop <boss name> *take item from hand*
					if(args.length == 2){
						String bossName = args[1];
						setBossDrop(bossName, p.getInventory().getItemInMainHand());
						p.sendMessage(ChatColor.GREEN + "Added: " + p.getInventory().getItemInMainHand().getType() + " to " + bossName + " drops!");
						return true;
					}
					
					else{
						invalidParameters(p);
						return false;
					}
				}
				
				//set xp dropped
				else if(args[0].equalsIgnoreCase("setxp")){
					// /createboss setxp <boss name> <xp amt>
					if(args.length == 3){
						String bossName = args[1];
						String xp = args[2];
						setBossXp(bossName, Integer.parseInt(xp));
						p.sendMessage(ChatColor.GREEN + "Set xp dropped to " + xp + " for " + bossName);
						return true;
					}
					
					else{
						invalidParameters(p);
						return false;
					}
				}
				
				//set health
				else if (args[0].equalsIgnoreCase("sethealth")){
					// /createboss sethealth <boss name> <health>
					if(args.length == 3){
						String bossName = args[1];
						String health = args[2];
						setBossHealth(bossName, Integer.parseInt(health));
						p.sendMessage(ChatColor.GREEN + "Set health to " + health + " for " + bossName);
						return true;
					}
					
					else{
						invalidParameters(p);
						return false;
					}
				}
				
				//set damage
				else if (args[0].equalsIgnoreCase("setdamage")){
					// /createboss setdamage <boss name> <damage>
					if(args.length == 3){
						String bossName = args[1];
						String damage = args[2];
						setBossDamage(bossName, Integer.parseInt(damage));
						p.sendMessage(ChatColor.GREEN + "Set damage to " + damage + " for " + bossName);
						return true;
					}
					
					else{
						invalidParameters(p);
						return false;
					}
				}
				
				//set armor
				else if (args[0].equalsIgnoreCase("setarmor")){
					// /createboss setarmor <boss name> *take armor from hand*
					if(args.length == 2){
						String bossName = args[1];
						setBossArmor(bossName, p.getInventory().getItemInMainHand());
						return true;
					}
				}
				//set weapon
			}
			else{
				invalidParameters(p);
				return false;
			}
		}
		return false;
	}

	private void setBossDamage(String bossName, int val) {
		checkDirectory();
		YMLOperations.ymlSet("damage", val, bossName);
	}

	private void setBossName(String bossName){
		checkDirectory();
		YMLOperations.ymlSet("name", bossName, bossName);
	}

	private void setBossType(String bossName, EntityType type){
		checkDirectory();
		YMLOperations.ymlSet("type", type.toString(), bossName);
	}
	
	private void setBossArmor(String bossName, ItemStack armor){
		checkDirectory();
		
		List<ItemStack> armorList = new ArrayList<ItemStack>();
		armorList.add(armor);
		
		YMLOperations.ymlSetList("armor", armorList, bossName);
	}
	
	private void setBossDrop(String bossName, ItemStack drop){
		checkDirectory();
		
		List<ItemStack> dropList = new ArrayList<ItemStack>();
		dropList.add(drop);
		
		YMLOperations.ymlSetList("drops", dropList, bossName);
	}
	
	private void setBossXp(String bossName, int xp){
		checkDirectory();
		
		YMLOperations.ymlSet("xp", xp, bossName);
	}
	
	private void setBossHealth(String bossName, int health){
		checkDirectory();
		
		YMLOperations.ymlSet("health", health, bossName);
	}
	
	private static void invalidParameters(Player p){
		p.sendMessage(ChatColor.RED + "Invalid parameters!");
		helpMsg(p);
	}

	//Makes sure the file has a place to be stored within its plugin folder
	private void checkDirectory(){
		File file = new File("plugins//TougherMobs//Bosses//");
		
		if(!file.exists()){
			file.mkdirs();
		}
	}
	
	private EntityType getEntityTypeFromString(String type){
		for(MobValues mv : MobValues.values()){
			if(mv.getEntityType().toString().equalsIgnoreCase(type)){
				return mv.getEntityType();
			}
		}
		
		return null;
	}

	public static void helpMsg(Player p){
		String title = "-=-=-=TougherMobs=-=-=-";
		String str = "";
		for(int i = 0; i < title.length(); i++){
			if(i % 2 == 0)
				str += ChatColor.AQUA + title.substring(i, i+1);
			else
				str += ChatColor.DARK_AQUA + title.substring(i, i+1);
		}
		
		p.sendMessage(str);
	}
}
