package com.tougher.mobs.v2.lidle.events.mobs.curr;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class CustomEnderman extends CustomMob{

	public static List<Integer> blackListedEndermanBlocks = new ArrayList<Integer>();
	public static boolean customMoves = true;
	
	public CustomEnderman() {
		super(EntityType.ENDERMAN);
	}

	@Override
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e) {
		if(playerDamagedEntity(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getEntity(), (Player) e.getDamager());
		}
	}

	@Override
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e) {
		if(entityDamagedPlayer(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getDamager(), (Player) e.getEntity());
			e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
		}
	}

}
