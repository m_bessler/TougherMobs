package com.tougher.mobs.v2.lidle.events.mobs.old;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class ZombieEvent implements Listener, TougherMobEvent {

	TougherMobs pl;
	
	public ZombieEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	@EventHandler
	public void zombieSpawnEvent(EntitySpawnEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getEntity() instanceof Zombie){
			if(TougherMobs.mobArmor){
				MobUtils.runGenerateArmor((LivingEntity) e.getEntity());
				MobUtils.runGenerateWeapons((LivingEntity) e.getEntity());
			}
		}
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Zombie){
				//Player damages zombie
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Zombie){
			if(e.getEntity() instanceof Player){
				//Zombie damages player
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				return;
			}
		}
	}
	
}
