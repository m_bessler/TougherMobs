package com.tougher.mobs.v2.lidle.events.mobs.old;

import org.bukkit.entity.ElderGuardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class ElderGuardianEvent implements Listener, TougherMobEvent {

	public static boolean custom_moves = true;
	
	TougherMobs pl;

	public ElderGuardianEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}

	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof ElderGuardian){
				//Player damages elder guardian
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;

				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}

	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof ElderGuardian){
			if(e.getEntity() instanceof Player){
				//Elder guardian damages player
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;

				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				return;
			}
		}
	}

}
