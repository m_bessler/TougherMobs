package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Witch;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PotionSplashEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class WitchEvent implements Listener {

	TougherMobs pl;
	
	public WitchEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Witch){
				//Player damages witch
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void potionSplashEvent(PotionSplashEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getEntity().getShooter() instanceof Witch){
			List<LivingEntity> list = (List<LivingEntity>) e.getAffectedEntities();
			for(int i = 0; i < list.size(); i++){
				if(list.get(i) instanceof Player){
					//Witch potion damages player
					if(MobUtils.isBoss((LivingEntity) e.getEntity().getShooter()))
						return;
					
					MobUtils.setMaxHealth((LivingEntity) e.getEntity().getShooter(), (Player) list.get(i));
					e.setIntensity((LivingEntity) list.get(i), 2.0);
					return;
				}
			}
		}
	}
	
	
	
}
