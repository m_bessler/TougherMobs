package com.tougher.mobs.v2.lidle.events.mobs.curr;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.players.PlayerUtils;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public abstract class CustomMob implements Listener{

	private EntityType entityType;
	protected List<CustomAttacks.Attacks> customAttacks = new ArrayList<CustomAttacks.Attacks>();

	public CustomMob(EntityType entityType){
		this.entityType = entityType;
		TougherMobs.pl.getServer().getPluginManager().registerEvents(this, TougherMobs.pl);
		TougherMobs.pl.getLogger().info("Enabled " + entityType.toString() + " events!");
	}

	@EventHandler
	public abstract void playerDamageEntity(EntityDamageByEntityEvent e);

	@EventHandler
	public abstract void entityDamagePlayer(EntityDamageByEntityEvent e);

	protected boolean playerDamagedEntity(EntityDamageByEntityEvent e){
		if(!canExecuteEvents(e.getEntity())) return false;

		if(e.getDamager() instanceof Player
				&& e.getEntityType() == entityType) return true;

		else return false;	
	}

	protected boolean entityDamagedPlayer(EntityDamageByEntityEvent e){
		if(!canExecuteEvents(e.getDamager())) return false;

		if(e.getDamager().getType() == entityType
				&& e.getEntity() instanceof Player) return true;

		else return false;	
	}

	protected void scaleMobHealthToPlayer(LivingEntity le, Player p){
		if(!(MobUtils.mobUUID.contains(le.getUniqueId().toString()))){
			int gearRating = PlayerUtils.getGearRating(p);
			int healthMultiplier = PlayerUtils.healthMultiplier(gearRating);
			int health = (int) le.getHealth() + healthMultiplier;
			le.setHealth(health);
			
			MobUtils.mobUUID.add(le.getUniqueId().toString());
		}
	}
	
	protected int scaleMobDamageToPlayer(Player p){
		int gearRating = PlayerUtils.getGearRating(p);
		int dmgMultiplier = PlayerUtils.dmgMultiplier(gearRating);
		
		return dmgMultiplier; 
	}
	
	protected boolean isSupportedEntity(Entity e){
		if(!canExecuteEvents(e)) return false;

		if(e.getType() == entityType) return true;

		return false;
	}

	private boolean canExecuteEvents(Entity e){
		if(!isSupportedWorld(e.getWorld())) return false;

		if(!(e instanceof LivingEntity)) return false;

		if(MobUtils.isBoss((LivingEntity) e)) return false;
		
		return true;
	}

	protected boolean isSupportedWorld(World w){
		return TougherMobs.isSupportedWorld(w.getName());
	}

	protected String getRandomCustomMove(){
		int size = customAttacks.size();
		if(size == 0) return "";
		int index = TougherMobs.rand.nextInt(size);

		return customAttacks.get(index).getAttackType();
	}
}
