package com.tougher.mobs.v2.lidle.events.mobs.old;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Stray;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class StrayEvent implements Listener, TougherMobEvent {

	TougherMobs pl;
	
	public StrayEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Stray){
				//Player damages stray
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Stray){
			if(e.getEntity() instanceof Player){
				//Stray damages player
				if(MobUtils.isBoss((LivingEntity) e.getDamager()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getDamager(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				return;
			}
		}
	}
	
}
