package com.tougher.mobs.v2.lidle.events.mobs.old;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.players.PlayerUtils;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class GhastEvent implements Listener, TougherMobEvent {

	TougherMobs pl;
	
	public GhastEvent(TougherMobs pl){
		this.pl = pl;
		pl.getServer().getPluginManager().registerEvents(this, pl);
	}
	
	//When a player damages a mob
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(e.getDamager() instanceof Player){
			if(e.getEntity() instanceof Ghast){
				//Player damages ghast
				if(MobUtils.isBoss((LivingEntity) e.getEntity()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) e.getEntity(), (Player) e.getDamager());
				return;
			}
		}
	}
	
	//When a mob damages a player
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e){
		if(!(TougherMobs.isSupportedWorld(e.getEntity().getWorld().getName()))) return; 
		
		if(isFireballFromGhast(e)){
			if(e.getEntity() instanceof Player){
				Fireball fb = (Fireball) e.getDamager();
				//Ghast fireball damages player
				if(MobUtils.isBoss((LivingEntity) fb.getShooter()))
					return;
				
				MobUtils.setMaxHealth((LivingEntity) fb.getShooter(), (Player) e.getEntity());
				e.setDamage(e.getDamage() + MobUtils.getDamage((Player) e.getEntity()));
				return;
			}
		}
	}
	
	//Checks to see if the fireball shot is from a Ghast
	//Pre-Condition: EntityDamageByEntity event is passed as a parameter
	//Post-Condition: Method extrapolates data from the event to see if fireball
	//was shot from a Ghast
	private boolean isFireballFromGhast(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Fireball){
			Fireball fb = (Fireball) e.getDamager();
			if(fb.getShooter() instanceof Ghast)
				return true;
			return false;
		}
		
		return false;
	}
	
	/**
	 * 15% Chance for a ghast fireball to spawn lava
	 * @param p - the player who is being attacked by the ghast
	 * */
	public void spawnLavaAttack(Player p){
		int gearRating = PlayerUtils.getGearRating(p);
		
		//If player has a gear rating over 70
		if(gearRating >= 70){
			Random rand = new Random();
			
			if(rand.nextInt(100) <= 15){
				p.getWorld().getBlockAt(p.getLocation()).setType(Material.LAVA);
			}
		}
		
	}
	
}
