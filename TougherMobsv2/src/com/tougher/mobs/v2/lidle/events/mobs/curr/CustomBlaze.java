package com.tougher.mobs.v2.lidle.events.mobs.curr;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.tougher.mobs.v2.lidle.main.TougherMobs;
import com.tougher.mobs.v2.lidle.utils.MobUtils;

public class CustomBlaze extends CustomMob{

	public static boolean customMoves = true;
	
	public CustomBlaze() {
		super(EntityType.BLAZE);
		customAttacks.add(CustomAttacks.Attacks.FIRE);
		customAttacks.add(CustomAttacks.Attacks.LAVA);
	}

	@Override
	@EventHandler
	public void playerDamageEntity(EntityDamageByEntityEvent e) {
		if(playerDamagedEntity(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getEntity(), (Player) e.getDamager());
		}
	}

	@Override
	@EventHandler
	public void entityDamagePlayer(EntityDamageByEntityEvent e) {
		if(entityDamagedPlayer(e)){
			scaleMobHealthToPlayer((LivingEntity) e.getDamager(), (Player) e.getEntity());
			e.setDamage(MobUtils.getDamage((Player) e.getEntity()));
			
			runAttacks((Player) e.getEntity());
		}
	}

	private void runAttacks(Player p){
		if(!customMoves) return;
		
		if(TougherMobs.rand.nextInt(100) + 1 <= 5){
			String move = getRandomCustomMove();
			CustomAttacks.executeCustomAttack(move, p);
		}
	}
	
}
