package com.tougher.mobs.v2.lidle.players;

import org.bukkit.Material;

public enum WeaponRatings {

	//SWORDS
	WOOD_SWORD(Material.WOOD_SWORD, 2),
	STONE_SWORD(Material.STONE_SWORD, 3),
	GOLD_SWORD(Material.GOLD_SWORD, 4),
	IRON_SWORD(Material.IRON_SWORD, 6),
	DIAMOND_SWORD(Material.DIAMOND_SWORD, 8),
	
	//AXES
	WOOD_AXE(Material.WOOD_AXE, 3),
	STONE_AXE(Material.STONE_AXE, 5),
	GOLD_AXE(Material.GOLD_AXE, 7),
	IRON_AXE(Material.IRON_AXE, 10),
	DIAMOND_AXE(Material.DIAMOND_AXE, 12),
	
	//MISC
	BOW(Material.BOW, 3),
	SHIELD(Material.SHIELD, 3);
	
	private Material material;
	private int weaponRating;
	
	WeaponRatings(Material material, int weaponRating){
		this.material = material;
		this.weaponRating = weaponRating;
	}

	public Material getMaterial() {
		return material;
	}

	public int getWeaponRating() {
		return weaponRating;
	}
	
}
